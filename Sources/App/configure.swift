import Vapor
import PostgresNIO

// configures your application
public func configure(_ app: Application) throws {
    try DBHelpers.initDB()

//    try app.register(PostgreSQLProvider(config: PostgreSQLDatabaseConfig()))
//    var databases = DatabasesConfig()
//    databases.add(database: postgresql, as: .psql)
//    services.register(databases)
    try routes(app)
}
